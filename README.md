# Xsolla Summer School FE stand
Приложение-стенд для студентов летней школы.

### Install
Для работы приложения необходимо установить [json-server](https://github.com/typicode/json-server)

```
npm install -g json-server
json-server --watch db.json
```

А также установить зависимости приложения
```npm install``` или ```yarn install```

Запуск
``` npm run dev``` или ```yarn dev```