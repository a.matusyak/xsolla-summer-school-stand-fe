import React, { Fragment } from 'react';

import ArticleParagraphs from '../paragraphs';
import './style.scss';

const ArtcileBody = ({description, sections}) => (
  <div className="post-body">
    
    {/* description */}
    {description && (<p>{description}</p>)}

    {sections.map((section, idx) => {
      <Fragment>
        {/* Item title */}
        <h2 key={idx}>{section.title}</h2>

        {/* Paragraph(s) */}
        <ArticleParagraphs paragraphs={section.paragraphs} />
      </Fragment>
    })}

  </div>
);

export default ArtcileBody;
