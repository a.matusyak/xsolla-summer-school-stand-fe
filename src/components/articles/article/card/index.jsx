import React from 'react';
import './style.scss';

const ArticleCard = ({card}) => (
  <div className="showcase__card post-card">
    <div className="post-card__image-wrapper">
      <div
        className="post-card__image"
        style={{backgroundImage: `url(${card.image})`}}
      ></div>
    </div>

    <div className="post-card__text-wrapper">
      <p className="post-card__category">{card.category}</p>

      <h3 className="post-card__title">{card.title}</h3>
    </div>
  </div>
);

export default ArticleCard;
