import React from 'react';
import './style.scss';

const IMAGE_LINK = "https://avatars.mds.yandex.net/get-media-platform/1851341/file_1552410932898/1240x1240";

const ArticleHeader = ({title, preview}) => (
  <div className="app__content">
    <main className="post-page">
      <article className="layout">
        <header className="post-header">
          <div className="post-header__top">
            <div className="post-header__text-wrapper">
              {title && <h1 className="post-header__title">{title}</h1>}
              <div className="post-header__preview">
                {preview && <p>{preview}</p>}
              </div>
            </div>

            <div className="post-header__image">
              <img src={IMAGE_LINK} />
            </div>
          </div>
        </header>
      </article>
    </main>
  </div>
);

export default ArticleHeader;
