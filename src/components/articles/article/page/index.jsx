import React, {Fragment} from 'react';

import ArticleHeader from '../header';
import ArticleBody from '../body';

const ArticlePage = ({article}) => {
  const {title, preview, description, sections} = article;

  return (
    <Fragment>
      <ArticleHeader title={title} preview={preview} />
      <ArticleBody description={description} sections={sections} />
    </Fragment>
  )
};

export default ArticlePage;
