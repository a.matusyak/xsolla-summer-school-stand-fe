import React, { Fragment } from 'react';

const ArticleParagraph = (paragraphs = []) => (
  <Fragment>
    {paragraphs.map((paragraph, idx) => {
      <p key={idx}>{paragraph}</p>
    })}
  </Fragment>
);

export default ArticleParagraph;