import React from 'react';
import { Link } from 'react-router-dom';

import ArticleCard from '../article/card';
import './style.scss';

const ArtilesList = ({cards}) => (
  <div className="app__content">
    <main className="main-page">
      <div className="layout">
        <div className="main-page__content">
          <div className="main-page__container">
            <div className="showcase">
              {cards && cards.map((card, idx) => (
                <Link to={`/article/${card.id}`} key={idx}>
                  <ArticleCard card={card} />
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
);

export default ArtilesList;
