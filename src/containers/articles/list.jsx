import React, {PureComponent} from 'react';
import {connect} from 'react-redux';

import ArticlesList from '../../components/articles/list';
import {fetchCards} from '../../reducers/cards';

class ArticlesContianer extends PureComponent {
  componentDidMount() {
    const {fetchCards} = this.props;

    fetchCards();
  }

  render() {
    const {cards} = this.props;

    return (
      <ArticlesList cards={cards} />
    )
  }
}

const mapStateToProps = state => ({
  cards: state.cards
});

const mapDispatchToProps = {
  fetchCards
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesContianer);
