import React, {PureComponent, Fragment} from 'react';
import {connect} from 'react-redux';

import {fetchArticle, getArticleById} from '../../reducers/articles';
import ArticlePage from '../../components/articles/article/page';

class ArticlePageContainer extends PureComponent {
  componentDidMount() {
    const {match, fetchArticle} = this.props;

    fetchArticle(match.params.id)
  }

  render() {
    const {article} = this.props;

    return (
      <Fragment>
        {article && <ArticlePage article={article} />}
      </Fragment>
    )
  }
}

const mapStateToProps = (store, props) => ({
  article: getArticleById(store, props.match.params.id)
});

const mapDispatchToProps = {
  fetchArticle,
  getArticleById
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlePageContainer);
