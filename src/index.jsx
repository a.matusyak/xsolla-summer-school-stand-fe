import React from 'react';
import ReactDOM from 'react-dom';

import Root from './layouts/root';

ReactDOM.render(
  <Root />,
  document.querySelector("#root")
);

if (module.hot) {
  module.hot.accept();
}
