import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import Header from '../components/header';
import ArticlesContainer from '../containers/articles/list';
import ArticlePageContainer from '../containers/articles/page';
import rootReducer from '../reducers';
import '../styles/app.scss';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const Root = () => (
  <Provider store={store}>
    <Router>
      <Fragment>
        <Header />

        <Route path="/" exact component={ArticlesContainer} />
        <Route path="/article/:id" component={ArticlePageContainer} />
      </Fragment>
    </Router>
  </Provider>
);

export default Root;
