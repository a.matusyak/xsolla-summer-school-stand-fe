import {articlesRequest} from '../services/articles';

const rootType = '@app/articles';
const ARTICLE_ADD = `${rootType}/add`;

const initialState = {};

export default (state = initialState, action) => {
  switch(action.type) {
    case ARTICLE_ADD:
      return {
        ...state,
        [action.payload.id]: action.payload
      }
    default:
      return state;
  }
};

// actions
export const addArticle = article => ({
  type: ARTICLE_ADD,
  payload: article
});

// action creators
export const fetchArticle = id => async dispatch => 
  articlesRequest(id)
    .then(response => dispatch(addArticle(response.data)));

// selectors
export const getArticleById = (state, id) => state.articles[id];