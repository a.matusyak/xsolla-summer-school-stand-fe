import {cardsRequest} from '../services/cards';

const rootType = '@app/articles';
const CARDS_SET = `${rootType}/set`;

const initialState = []

export default (state = initialState, action) => {
  switch (action.type) {
    case CARDS_SET:
      return [
        ...action.payload
      ]

    default:
      return state
  }
};

// actions
export const setCards = events => ({
  type: CARDS_SET,
  payload: events
});

// action creators
export const fetchCards = () => async (dispatch) =>
  cardsRequest
    .then(response => dispatch(setCards(response.data)));
