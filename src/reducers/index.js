import {combineReducers} from 'redux';

import cards from './cards';
import articles from './articles';

const rootReducer = combineReducers({
  cards,
  articles
});

export default rootReducer;
