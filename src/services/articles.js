import http from './http';

export const articlesRequest = id => http.get(`/articles/${id}`);
